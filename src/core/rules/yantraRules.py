class YantraRules:
    valid_yantras = ('demesne', 'environment', 'supernal verge', 'concentration',
                     'mantra', 'mudra', 'runes', 'tool', 'dedicated tool',
                     'material sympathy', 'representational sympathy',
                     'easy sacrament', 'medium sacrament', 'hard sacrament', 'persona 1',
                     'persona 2', 'persona 3', 'persona 4'
                     )

    @staticmethod
    def cast_time(time: str) -> int:
        time_type = {
            'ritual': -2,
            'reflexive': -1,
            'instant': 0
        }
        return time_type[time]

    @staticmethod
    def cast_time_word(amount: int) -> str:
        time_word = {
            -1: 'ritual',
            0: 'reflexive',
            1: 'instant'
        }

        if 1 > amount > -3:
            return time_word[amount]
        elif amount == 1:
            return '1 turn'
        elif amount > 0:
            return str(amount) + ' turns'

    @staticmethod
    def yantra_cast_time(yantra: str) -> int:
        case = {
            'demesne': 1,
            'environment': 1,
            'supernal verge': 1,
            'concentration': 2,
            'mantra': 2,
            'mudra': 1,
            'runes': -1,
            'tool': 1,
            'dedicated tool': 1,
            'material sympathy': 1,
            'representational sympathy': 1,
            'easy sacrament': 1,
            'medium sacrament': 1,
            'hard sacrament': 1,
            'persona 1': 1,
            'persona 2': 1,
            'persona 3': 1,
            'persona 4': 1
        }

        if YantraRules.valid_yantra(yantra):
            return case[yantra]

    @staticmethod
    def valid_yantra(yantra: str) -> bool:
        return yantra in YantraRules.valid_yantras

    @staticmethod
    def yantras_name(yantra: str) -> dict:
        from core.rules.diceRules import DiceRules

        if yantra in YantraRules.valid_yantras:
            return {'name': yantra,
                    'bonus': DiceRules.bonus_dice(yantra),
                    'time': YantraRules.cast_time_word(YantraRules.yantra_cast_time(yantra))}
        else:
            return {'error': 'Invalid yantra'}

    @staticmethod
    def bonus_dice(yantra: str, **keyword_parameters) -> int:
        if 'skill' in keyword_parameters:
            skill = keyword_parameters['skill']
        else:
            skill = 0

        case = {
            'demesne': 2,
            'environment': 1,
            'supernal verge': 2,
            'concentration': 2,
            'mantras': 2,
            'mantra': 2,
            'mudra': skill,
            'runes': 2,
            'tool': 1,
            'dedicated tool': 0,
            'material sympathy': 0,
            'representational sympathy': 1,
            'easy sacrament': 1,
            'medium sacrament': 2,
            'hard sacrament': 3,
            'persona 1': 1,
            'persona 2': 2,
            'persona 3': 3,
            'persona 4': 4
        }
        if YantraRules.valid_yantra(yantra):
            return case[yantra]
        else:
            return -1
