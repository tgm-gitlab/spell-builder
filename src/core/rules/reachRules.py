from functools import partial


class ReachRules:
    from core.classes.reach import Reach
    from core.classes.spell import Spell

    special_reach = ('advanced duration', 'advanced scale', 'advanced potency', 'instant',
                     'reflexive', 'sensory range', 'spell slots', 'spell factor')

    special_reach_effects = {
        'advanced duration': 'Move the spell from Standard to Advanced duration',
        'advanced scale': 'Move the spell from Standard to Advanecd duration',
        'advanced potency': 'Move the spell from Standard to Advanced potency',
        'instant': 'Move the spell from Ritual to Instant Casting Time',
        'reflexive': 'Move the spell from Instant Casting Time to Reflexive Casting Time',
        'sensory range': 'Move the spell from touch/self to sensory range',
        'spell slots': 'Cast a spell with no control spell slots remaining',
        'spell factor': 'Change the primary factor of a spell'
    }

    @staticmethod
    def nothing():
        pass

    @staticmethod
    def assign(spell: 'Spell', name: str, exp):
        if name == 'potency':
            spell.potency = exp
        if name == 'duration':
            spell.duration = exp
        if name == 'scale':
            spell.scale = exp
        if name == 'range':
            spell.range = exp

    @staticmethod
    def apply_reach(spell: 'Spell', reach: 'Reach'):
        application = {'advanced duration': partial(ReachRules.assign, spell, 'duration', "advanced"),
                       'advanced scale': partial(ReachRules.assign, spell, 'scale', "advanced"),
                       'advanced potency': partial(assign, spell, 'potency', "advanced"),
                       'instant': spell.set_instant,
                       'reflexive': spell.set_reflexive,
                       'sensory range': partial(ReachRules.assign, spell, 'range', "sensory range"),
                       'spell slots': ReachRules.nothing,
                       'spell factor': ReachRules.nothing

                    }

        if reach in ReachRules.special_reach:
            application[reach]()
