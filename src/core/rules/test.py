from functools import partial
from core.rules.spellRules import Spell
from core.classes.reach import Reach


def nothing():
    pass


def assign(spell: 'Spell', name: str, exp):
    if name == 'potency':
        spell.potency = exp
    if name == 'duration':
        spell.duration = exp
    if name == 'scale':
        spell.scale = exp
    if name == 'range':
        spell.range = exp


valid_yantras = ('demesne', 'environment', 'supernal verge', 'concentration',
                 'mantra', 'mudra', 'runes', 'tool', 'dedicated tool',
                 'material sympathy', 'representational sympathy',
                 'easy sacrament', 'medium sacrament', 'hard sacrament', 'persona 1',
                 'persona 2', 'persona 3', 'persona 4')

special_reach = ('advanced duration', 'advanced scale', 'advanced potency', 'instant',
                 'reflexive', 'sensory range', 'spell slots', 'spell factor')

special_reach_effects = {
    'advanced duration': 'Move the spell from Standard to Advanced duration',
    'advanced scale': 'Move the spell from Standard to Advanecd duration',
    'advanced potency': 'Move the spell from Standard to Advanced potency',
    'instant': 'Move the spell from Ritual to Instant Casting Time',
    'reflexive': 'Move the spell from Instant Casting Time to Reflexive Casting Time',
    'sensory range': 'Move the spell from touch/self to sensory range',
    'spell slots': 'Cast a spell with no control spell slots remaining',
    'spell factor': 'Change the primary factor of a spell'
}

valid_primaries = ('duration', 'scale', 'potency')


def cast_time(time: str) -> int:
    time_type = {
        'ritual': -2,
        'reflexive': -1,
        'instant': 0
    }
    return time_type[time]


def cast_time_word(amount: int) -> str:
    time_word = {
        -1: 'ritual',
        0: 'reflexive',
        1: 'instant'
    }

    if 1 > amount > -3:
        return time_word[amount]
    elif amount == 1:
        return '1 turn'
    elif amount > 0:
        return str(amount) + ' turns'


def yantra_cast_time(yantra: str) -> int:
    case = {
        'demesne': 1,
        'environment': 1,
        'supernal verge': 1,
        'concentration': 2,
        'mantra': 2,
        'mudra': 1,
        'runes': -1,
        'tool': 1,
        'dedicated tool': 1,
        'material sympathy': 1,
        'representational sympathy': 1,
        'easy sacrament': 1,
        'medium sacrament': 1,
        'hard sacrament': 1,
        'persona 1': 1,
        'persona 2': 1,
        'persona 3': 1,
        'persona 4': 1
    }

    if valid_yantra(yantra):
        return case[yantra]


def valid_yantra(yantra: str) -> bool:
    return yantra in valid_yantras


def bonus_dice(yantra: str, **keyword_parameters) -> int:
    if 'skill' in keyword_parameters:
        skill = keyword_parameters['skill']
    else:
        skill = 0

    case = {
        'demesne': 2,
        'environment': 1,
        'supernal verge': 2,
        'concentration': 2,
        'mantras': 2,
        'mantra': 2,
        'mudra': skill,
        'runes': 2,
        'tool': 1,
        'dedicated tool': 0,
        'material sympathy': 0,
        'representational sympathy': 1,
        'easy sacrament': 1,
        'medium sacrament': 2,
        'hard sacrament': 3,
        'persona 1': 1,
        'persona 2': 2,
        'persona 3': 3,
        'persona 4': 4
    }
    if valid_yantra(yantra):
        return case[yantra]
    else:
        return -1


def yantras_name(yantra: str) -> dict:
    if yantra in valid_yantras:
        return {'name': yantra,
                'bonus': bonus_dice(yantra),
                'time': cast_time_word(yantra_cast_time(yantra))}
    else:
        return {'error': 'Invalid yantra'}


def cast_time(spell: 'Spell'):
    if spell.instant:
        spell.time = -1
    else:
        spell.time = 0
        for yantra in spell.yantras:
            if yantra.time is not -1:
                spell.time = spell.time + yantra.time
            else:
                spell.time = -1
                break
    return cast_time_word(spell.time)


def valid_dice_pool(spell: 'Spell') -> bool:
    return True


def dice_pool(spell: 'Spell') -> bool:
    spell.pool = spell.caster.arcana + spell.caster.gnosis
    for yantra in spell.yantras:
        if yantra.name is 'mudra':
            spell.pool + bonus_dice(yantra.name, skill=spell.rote.skill)
        else:
            spell.pool = spell.pool + yantra.bonus

    spell.pool = spell.pool - spell.penalties
    if valid_dice_pool(spell):
        return True
    else:
        return False


def calculate_paradox(spell: 'Spell') -> tuple:
    paradoxical = False
    free_reach = (spell.caster.arcana - spell.spell_lvl) + 1
    if free_reach < 0:
        free_reach = 0
    paradox = len(spell.reach) - free_reach
    if paradox > 0:
        paradoxical = True
    res = (paradox, paradoxical)
    return res


def apply_reach(spell: 'Spell', reach: 'Reach'):
    application = {'advanced duration': partial(assign, spell, 'duration', "advanced"),
                   'advanced scale': partial(assign, spell, 'scale', "advanced"),
                   'advanced potency': partial(assign, spell, 'potency', "advanced"),
                   'instant': spell.set_instant,
                   'reflexive': spell.set_reflexive,
                   'sensory range': partial(assign, spell, 'range', "sensory range"),
                   'spell slots': nothing,
                   'spell factor': nothing

                }

    if reach in special_reach:
        application[reach]()
