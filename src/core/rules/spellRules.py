from core.classes.spell import Spell


class SpellRules:
    valid_primaries = ('duration', 'scale', 'potency')

    @staticmethod
    def get_yantras(spell: 'Spell') -> list:
        from core.rules.yantraRules import YantraRules

        res = []
        for yantra in spell.yantras:
            item = {'name': yantra.name,  'bonus': yantra.bonus, 'time': YantraRules.cast_time_word(yantra.time)}
            res.append(item)
        return res

    @staticmethod
    def in_yantras(spell: 'Spell', name: str) -> bool:
        for yantra in spell.yantras:
            if name == yantra.name:
                return True
        return False

    @staticmethod
    def get_reach(spell: 'Spell'):
        res = []
        for reach in spell.reach:
            item = {'name': reach.name, 'effect': reach.effect, 'cost': reach.cost}
            res.append(item)
        return res

    @staticmethod
    def set_instant(spell: 'Spell') -> bool:
        if len(spell.yantras) > 2 and not SpellRules.in_yantras(spell, 'runes'):
            spell.instant = True
            return True
        else:
            spell.instant = False
            return False

    @staticmethod
    def set_reflexive(spell: 'Spell') -> bool:
        if spell.instant and len(spell.yantras) > 2 and not SpellRules.in_yantras(spell, 'runes'):
            spell.reflexive = True
            return True
        else:
            spell.reflexive = False
            return False

    @staticmethod
    def cast_time(spell: 'Spell'):
        from core.rules.yantraRules import YantraRules

        if spell.instant:
            spell.time = -1
        else:
            spell.time = 0
            for yantra in spell.yantras:
                if yantra.time is not -1:
                    spell.time = spell.time + yantra.time
                else:
                    spell.time = -1
                    break
        return YantraRules.cast_time_word(spell.time)

    @staticmethod
    def format_range(range: str):
        if range is True:
            return 'Sensory range'
        elif range is False:
            return 'Touch'
        else:
            raise TypeError('Range must be a string, value: ' + range + ' is not a valid string.')

    @staticmethod
    def format_factor(factor: str):
        if factor is True:
            return 'Advanced'
        elif factor is False:
            return 'Standard'
        else:
            raise TypeError('Factor must be a string, value: ' + factor + ' is not a valid string.')

    @staticmethod
    def format_time(time: int):
        if time == -3:
            return 'Ritual'
        elif time == 0 or time == -1 or time == -2:
            res = ''
            if time == -2:
                res = 'Reflexive '
            res = res + str(time)
            return res
        else:
            raise TypeError('Time must be an int, value: ' + str(time) + ' is not a valid string.')
