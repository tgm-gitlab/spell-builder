class DiceRules:
    from core.rules.spellRules import Spell
    # from core.classes.yantra import Yantra

    @staticmethod
    def valid_dice_pool(spell: 'Spell') -> bool:
        return True

    @staticmethod
    def dice_pool(spell: 'Spell') -> bool:
        from core.rules.yantraRules import YantraRules

        spell.pool = spell.caster.arcana + spell.caster.gnosis
        for yantra in spell.yantras:
            if yantra.name is 'mudra':
                spell.pool + YantraRules.bonus_dice(yantra.name, skill=spell.rote.skill)
            else:
                spell.pool = spell.pool + yantra.bonus

        spell.pool = spell.pool - spell.penalties
        if DiceRules.valid_dice_pool(spell):
            return True
        else:
            return False

    @staticmethod
    def calculate_paradox(spell: 'Spell') -> tuple:
        paradoxical = False
        free_reach = (spell.caster.arcana - spell.spell_lvl) + 1
        if free_reach < 0:
            free_reach = 0
        paradox = len(spell.reach) - free_reach
        if paradox > 0:
            paradoxical = True
        res = (paradox, paradoxical)
        return res
