class Rote:
    name = ''
    skill = 0
    author = ''
    grimoires = False

    def __init__(self, name: str, skill: int, grim: bool, author: str):
        self.name = name
        self.skill = skill
        self.grimoires = grim
        self.author = author
