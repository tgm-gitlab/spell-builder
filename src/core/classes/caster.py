class Caster:
    def __init__(self, arcana: int, gnosis: int, aim_skill: int, rote_skill: int):
        self._arcana = arcana
        self._gnosis = gnosis
        self._aim_skill = aim_skill
        self._rote_skill = rote_skill

    @property
    def arcana(self):
        return self._arcana

    @arcana.setter
    def arcana(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._arcana = int(value)
            else:
                self._arcana = 0
        else:
            self._arcana = value

    @property
    def gnosis(self):
        return self._gnosis

    @gnosis.setter
    def gnosis(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._gnosis = int(value)
            else:
                self._gnosis = 0
        else:
            self._gnosis = value

    @property
    def aim_skill(self):
        return self._aim_skill

    @aim_skill.setter
    def aim_skill(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._aim_skill = int(value)
            else:
                self._aim_skill = 0
        else:
            self._aim_skill = value

    @property
    def rote_skill(self):
        return self._rote_skill

    @rote_skill.setter
    def rote_skill(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._rote_skill = int(value)
            else:
                self._rote_skill = 0
        else:
            self._rote_skill = value