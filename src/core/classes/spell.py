from core.classes.caster import Caster


class Spell:
    def __init__(self, caster: 'Caster'):
        self._caster = caster
        self._penalties = 0
        self._pool = 0
        self._spell_lvl = 0
        self._time = 0
        self._primary = 'potency'
        self._type = 'ruling'

    @property
    def caster(self):
        return self._caster

    @caster.setter
    def caster(self, value):
        if not isinstance(value, Caster):
            raise TypeError('Caster must of type Caster')
        self._caster = value

    yantras = []
    reach = []

    @property
    def pool(self):
        return self._pool

    @pool.setter
    def pool(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._pool = int(value)
            else:
                self._pool = 0
        else:
            self._pool = value

    @property
    def penalties(self):
        return self._penalties

    @penalties.setter
    def penalties(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._penalties = int(value)
            else:
                self._penalties = 0
        else:
            self._penalties = value

    @property
    def spell_lvl(self):
        return self._spell_lvl

    @spell_lvl.setter
    def spell_lvl(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._spell_lvl = int(value)
            else:
                self._spell_lvl = 0
        else:
            self._spell_lvl = value

    @property
    def time(self):
        return self._time

    @time.setter
    def time(self, value):
        if not isinstance(value, int):
            if value.isdigit():
                self._time = int(value)
            else:
                self._time = 0
        else:
            self._time = value

    @property
    def primary(self):
        return self._primary

    @primary.setter
    def primary(self, value):
        if not isinstance(value, str):
            raise TypeError('Primary must be a string')
        self._primary = value

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        if not isinstance(value, str):
            raise TypeError('Primary must be a string')
        self._type = value

    duration = False
    scale = False
    potency = False
    range = False
    instant = False
    reflexive = False


