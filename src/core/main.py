from core.rules.spellRules import Spell
from core.classes.caster import Caster
from core.spellApi import Api
import webview
import threading

caster = Caster(0, 0, 0, 0)
spell = Spell(caster)

htmlfile = open('webapp/index.html', 'r')
html = htmlfile.read()


def create_app():
    webview.load_html(html)


if __name__ == '__main__':
    t = threading.Thread(target=create_app)
    t.start()

    api = Api(spell)
    webview.create_window(title='Spell Builder', js_api=api, width=1400, height=700, min_size=(1400, 700))
