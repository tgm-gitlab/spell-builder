from core.rules.spellRules import Spell
from core.classes.yantra import Yantra
from core.classes.reach import Reach
from core.classes.caster import Caster
from core.rules.diceRules import DiceRules
from core.rules.spellRules import SpellRules


# noinspection PyPep8Naming
class Api:
    spell = Spell(Caster(0, 0, 0, 0))
    spell_skills = ('pool', 'penalties', 'time', 'primary', 'spell_lvl', 'type')
    caster_skills = ('arcana', 'gnosis', 'aim_skill', 'rote_skill')

    def __init__(self, spell):
        if spell is not None:
            self.spell = spell

    def castTimeWord(self, amount: int) -> str:
        return self.spell.cast_time_word(amount)

    def castTime(self, time: str) -> int:
        return self.spell.cast_time(time)

    def validYantra(self, yantra: str) -> bool:
        return self.spell.valid_yantras(yantra)

    def bonusDice(self, yantra: str) -> int:
        return self.spell.bonus_dice(yantra)

    def dicePool(self, params) -> dict:
        DiceRules.dice_pool(self.spell)
        SpellRules.cast_time(self.spell)
        res = {
            'pool': self.spell.pool,
            'time': SpellRules.format_time(self.spell.time),
            'range': SpellRules.format_range(self.spell.range),
            'scale': SpellRules.format_factor(self.spell.scale),
            'duration': SpellRules.format_factor(self.spell.duration),
            'potency': SpellRules.format_factor(self.spell.potency)
            }

        return res

    def yantraName(self, yantra) -> dict:
        from core.rules.yantraRules import YantraRules
        return YantraRules.yantras_name(yantra)

    def push(self, field, value):
        if field in self.caster_skills:
            setattr(self.spell.caster, field, value)
        elif field in self.spell_skills:
            setattr(self.spell, field, value)
        else:
            raise TypeError('Cannot set value: ' + value + ' of unknown field. ' + field)

    def pull(self, field):
        if field in self.caster_skills:
            getattr(self.spell.caster, field)
        elif field in self.spell_skills:
            getattr(self.spell, field)
        else:
            raise TypeError('Cannot set value of unknown field. ' + field)

    def setSpell(self, params) -> None:
        for param in params:
            self.push(param, params[param])

    def getSpell(self, params) -> dict:
        spell_state = {'arcana': self.spell.caster.arcana,
                       'gnosis': self.spell.caster.gnosis,
                       'penalties': self.spell.penalties,
                       'yantras': SpellRules.get_yantras(self.spell),
                       'spell_lvl': self.spell.spell_lvl,
                       'reach': SpellRules.get_reach(self.spell),
                       'primary': self.spell.primary,
                       'type': self.spell.type,
                       'rote_skill': self.spell.caster.rote_skill,
                       'aim_skill': self.spell.caster.aim_skill,
                       }
        return spell_state

    def newYantra(self, yantra_name: str) -> bool:
        from core.rules.yantraRules import YantraRules

        if yantra_name in YantraRules.valid_yantras:
            for yantra in self.spell.yantras:
                if yantra_name == yantra.name:
                    return True

            new_yantra = Yantra(yantra_name, YantraRules.bonus_dice(yantra_name), YantraRules.yantra_cast_time(yantra_name))
            self.spell.yantras.append(new_yantra)
            return True
        else:
            return False

    def delYantra(self, yantra_name: str) -> bool:
        for yantra in self.spell.yantras:
            if yantra_name == yantra.name:
                self.spell.yantras.remove(yantra)
                return True
        return False

    def newReach(self, reach_name: str, **keyword_parameters) -> bool:
        from core.rules.reachRules import ReachRules

        if reach_name == '':
            return False
        effect = ''
        cost = ''

        if reach_name == 'instant' and self.spell.in_yantras('runes'):
            effect = 'Has no positive effect'
            cost = 'None'
        elif reach_name in ReachRules.special_reach:
            effect = ReachRules.special_reach_effects[reach_name]
            cost = 'None'
        else:
            if 'effect' in keyword_parameters:
                effect = keyword_parameters['effect']
            else:
                effect = 'None'
            if 'cost' in keyword_parameters:
                cost = keyword_parameters['cost']
            else:
                cost = 'None'
        self.spell.apply_reach(reach_name)
        new_reach = Reach(reach_name, effect, cost)
        self.spell.reach.append(new_reach)
        return True

    def delReach(self, reach_name: str) -> bool:
        for reach in self.spell.reach:
            if reach_name == reach.name:
                self.spell.resetReach(reach)
                self.spell.reach.remove(reach)
                return True
        return False

    def calculateParadoxDice(self):
        return self.spell.calculate_paradox()
