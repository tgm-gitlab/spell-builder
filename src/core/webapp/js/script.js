// On load function
window.addEventListener("load", function(){
    setTimeout(() => { init(); }, 25);
});

function init() {
    writeFields();
    assemblePool();
}


let set = (field_id, value) => {
    document.getElementById(field_id).value = value;
}

let get = (field_id) => {
    return document.getElementById(field_id).value;
}

function updateField(field) {
    params = {}
    switch(field){
        case 'arcana':
            params = {'arcana': document.getElementById('arcana').value};
            break;
        case 'gnosis':
            params = {'gnosis': document.getElementById('gnosis').value};
            break;
        case 'penalties':
            params = {'penalties': document.getElementById('penalties').value};
            break;
        case 'aim_skill':
            params = {'aim_skill': document.getElementById('aim_skill').value};
            break;
        case 'rote_skill':
            params = {'rote_skill': document.getElementById('rote_skill').value};
            break;
        case 'spell_lvl':
            params = {'spell_lvl': document.getElementById('spell_lvl').value};
        case 'primary':
            params = {'primary': document.getElementById('primary').value}
        default:
            break;
    }

   pywebview.api.setSpell(params).then((done) => {});

}

function assemblePool(){
    let arcana = get('arcana'); // document.getElementById('arcana').value;
    let gnosis = get('gnosis'); // document.getElementById('gnosis').value;
    let penalties = get('penalties'); // document.getElementById('penalties').value;
    let rote_skill = get('rote_skill'); // document.getElementById('skill').value;
    let aim_skill = get('aim_skill');
    let spell_lvl = get('spell_lvl'); // document.getElementById('spellLvl').value;
    let primary = get('primary'); // document.getElementById('primary').value;
    let type = get('type');

    let params = {'arcana': arcana,
                  'gnosis': gnosis,
                  'penalties': penalties,
                  'rote_skill': rote_skill,
                  'aim_skill': aim_skill,
                  'spell_lvl': spell_lvl,
                  'primary': primary,
                  'type': type}


    pywebview.api.setSpell(params).then(() => {
        pywebview.api.dicePool().then((res) => {
            document.querySelector('.pool').innerHTML = res.pool;
            document.querySelector('.time').innerHTML = res.time;
            document.querySelector('.range').innerHTML = res.range;
            document.querySelector('.scale').innerHTML = res.scale;
            document.querySelector('.duration').innerHTML = res.duration;
            document.querySelector('.potency').innerHTML = res.potency;
            writeFields();
        });
    });
}

function writeFields() {
    pywebview.api.getSpell().then((res) => {
        set('arcana', res.arcana);
        set('gnosis', res.gnosis);
        set('penalties', res.penalties);
        set('rote_skill', res.rote_skill);
        set('aim_skill', res.aim_skill);
        set('spell_lvl', res.spell_lvl);
        set('primary', res.primary);
        set('type', res.type);

        populateYantraTable(res.yantras);
        populateReachTable(res.reach)
    });
}

function addRow(table, first, second, third) {
    let row = table.insertRow(-1);

    let firstCell = row.insertCell(0);
    firstCell.innerHTML = first;
    firstCell.className = "cell";

    let secondCell = row.insertCell(1);
    secondCell.innerHTML = second;
    secondCell.className = "cell";

    let thirdCell = row.insertCell(2);
    thirdCell.innerHTML = third;
    thirdCell.className = "cell";

    let remBtn = document.createElement('button');
    let t = document.createTextNode('\u2613');
    remBtn.appendChild(t);

    let remCell = row.insertCell(3);

    if (table.id == "yantraTable") {
        remBtn.onclick = () => {remYantra(first, table.id, row.rowIndex - 1)}
    }
    else if (table.id == "reachTable") {
        remBtn.onclick = () => {remReach(first, table.id, row.rowIndex - 1)}
    }

    remCell.appendChild(remBtn);
    remCell.className = "cell small";
}

function addRowButton(table, mode){
    let row = table.insertRow(-1);

    if (mode == "reach"){

        let firstInput = document.createElement('input');
        firstInput.className = "txtfld";
        firstInput.id = table.id + 'first';

        let firstCell = row.insertCell(0);
        firstCell.className = "cell";
        firstCell.appendChild(firstInput);

        let secondInput = document.createElement('input');
        secondInput.className = "txtfld";
        secondInput.id = table.id + 'second';

        let secondCell = row.insertCell(1);
        secondCell.className = "cell";
        secondCell.appendChild(secondInput);

        let thirdInput = document.createElement('input');
        thirdInput.className = "txtfld";
        thirdInput.id = table.id + 'third';

        let thirdCell = row.insertCell(2);
        thirdCell.className = "cell";
        thirdCell.appendChild(thirdInput);

        let remBtn = document.createElement('button');
        let t = document.createTextNode('\u271A');
        remBtn.appendChild(t);

        let remCell = row.insertCell(3);

        remBtn.onclick = () => {newReach(table, row.rowIndex -1);}

        remCell.appendChild(remBtn);
        remCell.className = "cell small";
    }
    else if (mode == "yantra") {

        let firstInput = document.createElement('input');
        firstInput.className = "txtfld";
        firstInput.id = table.id + 'first';

        let firstCell = row.insertCell(0);
        firstCell.className = "cell";
        firstCell.appendChild(firstInput);

        let secondCell = row.insertCell(1);
        secondCell.className = "cell";

        let thirdCell = row.insertCell(2);
        thirdCell.className = "cell";

        let remBtn = document.createElement('button');
        let t = document.createTextNode('\u271A');
        remBtn.appendChild(t);

        let remCell = row.insertCell(3);

        remBtn.onclick = () => {newYantra(row.cells[0]);}

        remCell.appendChild(remBtn);
        remCell.className = "cell small";
    }
    else{
        window.alert("Populating tables failed. Supply proper table mode.");
    }
}

function populateYantraTable(yantras){
    var new_tbody = document.createElement('tbody');
    var old_tbody = document.getElementById('yantraTable');

    var i;
    for(i = 0; i < yantras.length; i++){
        addRow(new_tbody, yantras[i].name, yantras[i].bonus, yantras[i].time);
    }

    new_tbody.id = 'yantraTable';
    addRowButton(new_tbody, 'yantra');
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);

}

function populateReachTable(reach){
    var new_tbody = document.createElement('tbody');
    var old_tbody = document.getElementById('reachTable');

    var i;
    for(i = 0; i < reach.length; i++){
        addRow(new_tbody, reach[i].name, reach[i].effect, reach[i].cost);
    }

    new_tbody.id = 'reachTable';
    addRowButton(new_tbody, 'reach');
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);

}


function newYantra(name){
    pywebview.api.newYantra(name).then((res) => {
        assemblePool();
    });
}

function remYantra(name, tableID, rowIndex){
    pywebview.api.delYantra(name).then(() => {
        document.getElementById(tableID).deleteRow(rowIndex);
        assemblePool();
    });
}

function newReach(table, rowIndex){
    let name = table.rows[rowIndex].cells[0]
    let effect = table.rows[rowIndex].cells[1]
    let bonus =  table.rows[rowIndex].cells[2]

    pywebview.api.newReach(name, effect, bonus).then((res) => {
        assemblePool();
    });
}

function remReach(name, tableID, rowIndex){
    pywebview.api.delReach(name).then(() => {
        document.getElementById(tableID).deleteRow(rowIndex);
        assemblePool();
    });
}

//function chartTable(advanced, factor) {
//    var id = ''
//
//    switch(factor){
//        case 'duration':
//            id = 'durationChart';
//            break;
//        case 'scale':
//            id = 'scaleChart';
//            break;
//        default:
//            return false;
//    }
//
//    var table = document.getElementById(id);
//
//    if id === 'duration'{
//       var row1
//       var row2
//       var row3
//       var row4
//       var row5
//    }
//}